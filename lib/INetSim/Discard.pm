# -*- perl -*-
#
# INetSim::Discard - Base package for Discard::TCP and Discard::UDP
#
# (c)2007-2019 Thomas Hungenberg, Matthias Eckert
#
#############################################################

package INetSim::Discard;

use strict;
use warnings;
use base qw(INetSim::GenericServer);

# no shared functions

1;
#
