# -*- perl -*-
#
# INetSim::Time - Base package for Time::TCP and Time::UDP
#
# (c)2007-2019 Thomas Hungenberg, Matthias Eckert
#
#############################################################

package INetSim::Time;

use strict;
use warnings;
use base qw(INetSim::GenericServer);

# no shared functions

1;
#
