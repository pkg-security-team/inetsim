# -*- perl -*-
#
# INetSim::Daytime - Base package for Daytime::TCP and Dayime::UDP
#
# (c)2007-2019 Thomas Hungenberg, Matthias Eckert
#
#############################################################

package INetSim::Daytime;

use strict;
use warnings;
use base qw(INetSim::GenericServer);

# no shared functions

1;
#
