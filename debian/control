Source: inetsim
Section: utils
Priority: optional
Maintainer: Debian Security Tools <team+pkg-security@tracker.debian.org>
Uploaders: GengYu Rao <zouyoo@outlook.com>
Build-Depends: debhelper-compat (= 13), gcc-mingw-w64-i686, init-system-helpers
Standards-Version: 4.6.1
Rules-Requires-Root: no
Homepage: https://www.inetsim.org/index.html
Vcs-Git: https://salsa.debian.org/pkg-security-team/inetsim.git
Vcs-Browser: https://salsa.debian.org/pkg-security-team/inetsim

Package: inetsim
Architecture: all
Depends: ${misc:Depends}, ${perl:Depends}, libnet-server-perl, libnet-dns-perl, libipc-shareable-perl, libdigest-sha-perl, lsb-base, adduser, openssl
Pre-Depends: ${misc:Pre-Depends}
Recommends: libio-socket-ssl-perl, libnfqueue-perl, iptables
Description: Software suite for simulating common internet services
 INetSim is a software suite for simulating common internet services
 in a lab environment, e.g. for analyzing the network behaviour of
 unknown malware samples.
 .
 INetSim supports simulation of the following services:
 HTTP, SMTP, POP3, DNS, FTP, NTP, TFTP, IRC, Ident, Finger, Syslog,
 'Small servers' (Daytime, Time, Echo, Chargen, Discard, Quotd)
 .
 Additional features:
  * Faketime
  * Connection redirection
  * Detailed logging and reports
  * TLS/SSL support for several services
